package org.artoolkit.ar.samples.ARItaricon;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.threed.jpct.Config;
import com.threed.jpct.World;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import de.tud.sacc.ar.ARItaricon.AlfabetRestClient;
import de.tud.sacc.ar.ARItaricon.ModelLoader;
import de.tud.sacc.ar.ARItaricon.datamodel.Device;
import de.tud.sacc.ar.ARItaricon.scene.DeviceTrackableObject3D;

public class MainActivity extends ArJpctActivity {

    List<DeviceTrackableObject3D> allObjects3d = new LinkedList<DeviceTrackableObject3D>();
    Stack<DeviceTrackableObject3D> freeObjects3d = new Stack<DeviceTrackableObject3D>();
    Map<String, DeviceTrackableObject3D> deviceTrackableObjMap = new HashMap<String, DeviceTrackableObject3D>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchPatterns(this.getAssets(), "Data");
        ModelLoader.getInstance().initModelsFromAssets(this.getAssets(), "Data/models");
        setContentView(R.layout.main);
        AlfabetRestClient.getInstance().addListener(new AlfabetRestClient.DevicesUpdatedListener() {
            @Override
            public void onUpdated(Map<String, Device> _devices) {
                updateDeviceAssignement(_devices);
            }
        });
    }

    /**
     * This method loads all patterns, that can be found at path.
     *
     * @param mgr       An AssetManager Instance must be passed
     * @param path
     */
    private void searchPatterns(AssetManager mgr, String path) {
        try {
            String list[] = mgr.list(path);
            if (list != null)
                for (int i=0; i<list.length; ++i) {
                        if(list[i].startsWith("patt.")) {
                            String patternName = ("single;"+ path + "/"+ list[i]+ ";80");
                            DeviceTrackableObject3D obj3d =  new DeviceTrackableObject3D(patternName);
                            freeObjects3d.push(obj3d);
                            allObjects3d.add(obj3d);
                        }
                    }
        } catch (IOException e) {
            Log.v("List error:", "can't list" + path);
        }
    }

    /**
     * assigns each {@link Device} (data-model)
     * if there are markers left in the {@link freeObject3D} Stack
     *
     * @param deviceMap
     */
    private void updateDeviceAssignement(Map<String, Device> deviceMap) {
        for(Iterator<String> devIdIter = deviceMap.keySet().iterator(); devIdIter.hasNext();) {
            String devId = devIdIter.next();
            if(deviceTrackableObjMap.containsKey(devId)) {

            } else if(!freeObjects3d.empty()){
                DeviceTrackableObject3D tckobj = freeObjects3d.pop();
                tckobj.setDevice(deviceMap.get(devId));

                /*Object3D object3D = Primitives.getCube(60);
                object3D.setTransparency(10);
                object3D.setTransparencyMode(Object3D.TRANSPARENCY_MODE_DEFAULT);
                object3D.rotateY((float) Math.PI / 4);
                object3D.setOrigin(new SimpleVector(0, 0, 60));
                tckobj.addChild(object3D);*/
                deviceTrackableObjMap.put(devId, tckobj);
            }
        }
    }

    /**
     * Use the FrameLayout in this Activity's UI.
     */
    @Override
    protected FrameLayout supplyFrameLayout() {
        return (FrameLayout)this.findViewById(R.id.mainLayout);
    }

    public void configureWorld(World world) {
        Config.farPlane = 2000;
        world.setAmbientLight(255, 255, 255);
    }

    /**
     * will be called whenever a touch event occurs. Currently
     * it only triggers a reload of the data from the REST-Service.
     *
     * @param me
     * @return
     */
    public boolean onTouchEvent(MotionEvent me) {

        if (me.getAction() == MotionEvent.ACTION_DOWN) {
            AlfabetRestClient.getInstance().fetchDevices();
            return true;

        }

        if (me.getAction() == MotionEvent.ACTION_UP) {

            return true;
        }

        if (me.getAction() == MotionEvent.ACTION_MOVE) {

            return true;
        }

        try {
            Thread.sleep(15);
        } catch (Exception e) {
            // No need for this...
        }

        return super.onTouchEvent(me);
    }



    protected void populateTrackableObjects(List<TrackableObject3d> list) {
        for (DeviceTrackableObject3D obj : allObjects3d ) {
            list.add(obj);
        }
    }

}