package org.artoolkit.ar.samples.ARItaricon;

/**
 * Created by portales on 16/11/15.
 */
public interface OnVisibilityChangeListener {
    void onVisibilityChanged(boolean markerVisible);
}
