package de.tud.sacc.ar.ARItaricon;

//TODO: menu options in GUI for each config options
public class ARItariconConfig {
	private static ARItariconConfig instance = null;
	
	private ARItariconConfig() {
		
	}
	
	public static ARItariconConfig getInstance() {
		if(instance == null)
			instance = new ARItariconConfig();
		return instance;
	}
	
	public String getRestUrl() {
		return rest_url;
	}

	public void setRestUrl(String rest_url) {
		this.rest_url = rest_url;
	}

	private String  rest_url = /*"http://192.168.178.129:7000/";/*/ "https://services.itaricon.de/iib/visit/";
}
