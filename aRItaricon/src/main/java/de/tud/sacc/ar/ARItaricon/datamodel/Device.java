package de.tud.sacc.ar.ARItaricon.datamodel;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


/**
 * datamodel for a device in the Alfabet database
 * created from the JSON that is returned by the REST-Service
 */
public class Device {
	public final static String TAG = "models.Device";

	private String deviceName;
	private String deviceID;
	private String stereotype;
	private Map<String, Application> apps = new HashMap<String, Application>();
	private List<DeviceChangeListener> changeListerner = new ArrayList<DeviceChangeListener>();
	
	public Device(String _id, JSONObject obj) {
		this.deviceID = _id;
		updateFromJSON(obj);
	}
	
	/**
	 * Will set deviceName and stereotype of the device according to the
	 * JSON Structure passed to the function, everything else
	 * will be untouched
	 * @param obj
	 */
	
	public void updateFromJSON(JSONObject obj) {
		try {
			this.deviceName = obj.getString("DeviceName");
			this.stereotype = obj.getString("StereoType");
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing the Device JSON Object " + e.getMessage());
		}
		try {
			JSONObject appsObj = obj.getJSONObject("Apps");
			Set<String> keys = new HashSet<String>(apps.keySet());
			for(Iterator<String> devObjIter = appsObj.keys(); devObjIter.hasNext();) {
				String objId = devObjIter.next();
				JSONObject appObj = appsObj.getJSONObject(objId);
				if(apps.containsKey(objId)) {
					keys.remove(objId);
					apps.get(objId).updateFromJSON(appObj);
				} else {
					apps.put(objId, new Application(objId, appObj));
				}
			}
			//now delete all apps that possibly have gone (if not in JSONResponseObject)
			for(String key : keys) {
				apps.remove(key);
			}
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
		}
		informAboutChange();
	}
	
	public String getDeviceName() {
		return deviceName;
	}
	
	public String getDeviceID() {
		return deviceID;
	}
	
	public String  getStereotype() {
		return stereotype;
	}

	/**
	 * registers a listener to listen for changes of the data-model
	 *
	 * @param listener
     */
	public void addListener(DeviceChangeListener listener) {
		changeListerner.add(listener);
	}


	/**
	 * unregister a change listener
	 *
	 * @param listener
     */
	public void removeListener(DeviceChangeListener listener) {
		changeListerner.remove(listener);
	}

	private void informAboutChange() {
		for(DeviceChangeListener listener : changeListerner) {
			listener.onChange();
		}
	}

	/**
	 * Calculates the maximum Criticallity of all {@link Application} running on the {@link Device}
	 * @return maximum App Criticallity
     */
	public int getMaximumAppCriticallity() {
		int crit = 0;
		for(Application app : apps.values()) {
			crit = (app.getMaxCrit() > crit ) ? app.getMaxCrit() : crit;
		}
		return crit;
	}
	
	public Map<String, Application> getApps() {
		return apps;
	}

	public interface DeviceChangeListener extends EventListener  {
		void onChange();
	}

}
