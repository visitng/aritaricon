package de.tud.sacc.ar.ARItaricon.scene;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;

import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;

import org.artoolkit.ar.samples.ARItaricon.TrackableObject3d;

import de.tud.sacc.ar.ARItaricon.ModelLoader;
import de.tud.sacc.ar.ARItaricon.datamodel.Device;

/**
 * Special trackable object, that displays a model and some additional
 * information about a device (data-model)
 */
public class DeviceTrackableObject3D extends TrackableObject3d {
    public final static String TAG = "DeviceTrackableObject3D";
    private Device device;
    private final int CRIT_TRESHOLD = 4;

    public DeviceTrackableObject3D(String _markerID) {
        super(_markerID);
    }

    public Bitmap textToBitmap(String text, float textSize, int textColor) {
        TextPaint paint = new TextPaint();
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent();



        int width = (int)Math.pow(2,Math.ceil(Math.log(paint.measureText(text) + 0.5f)/Math.log(2)));
        int height = (int)Math.pow(2, Math.ceil(Math.log(baseline + paint.descent() + 0.5f)/Math.log(2)));

        int size = (width > height) ? width : height;

        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        StaticLayout textLayout = new StaticLayout(text, paint, size, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        textLayout.draw(canvas);
        //canvas.drawText(text, 0, baseline, paint);
        return bitmap;
    }

    public void setDevice(Device _device) {
        this.device = _device;
        updateDeviceSceneGraph();
        this.device.addListener(new Device.DeviceChangeListener() {
            @Override
            public void onChange() {
                updateDeviceSceneGraph();
            }
        });

    }

    public void updateDeviceSceneGraph() {
        int crit = device.getMaximumAppCriticallity();
        String modelName = "itaricon2";
        if(crit <= CRIT_TRESHOLD) {
            modelName = "minion2";
        }

        Texture texture = new Texture(textToBitmap(device.getDeviceID() + "\n" + device.getDeviceName() + "\n" + device.getMaximumAppCriticallity(), 60, Color.YELLOW), true);
        //TextureManager.getInstance().addTexture("box", new Texture("textures" + File.separatorChar + "box.jpg"));
        String textureID = "label" + device.getDeviceID();
        if(TextureManager.getInstance().containsTexture(textureID)) {
            TextureManager.getInstance().replaceTexture(textureID, texture);
        } else {
            TextureManager.getInstance().addTexture(textureID, texture);
        }

        Object3D plane = Primitives.getPlane(60,2);
        plane.setTexture(textureID);
        plane.setTransparency(10);
        plane.setTransparencyMode(Object3D.TRANSPARENCY_MODE_DEFAULT);

        plane.rotateY((float) Math.PI );
        plane.rotateZ((float) Math.PI );
        //plane.rotateZ((float) Math.PI);

        plane.setOrigin(new SimpleVector(0,0,5));
        plane.build();


        Object3D[] object3D2 = ModelLoader.getInstance().getModel(modelName);
        if(modelName != null) {
            this.clearChildren();
    /*        Object3D object3D = Primitives.getCube(60);
            object3D.setTransparency(10);
            object3D.setTransparencyMode(Object3D.TRANSPARENCY_MODE_DEFAULT);
            object3D.rotateY((float) Math.PI / 4);
            object3D.setOrigin(new SimpleVector(0, 0, 60));
            this.addChild(object3D);*/

            for (int i = 0; i < object3D2.length; i++) {
                this.addChild(object3D2[i].cloneObject());
            }
        } else {
            Log.v(TAG, "Could not find a model named" + modelName);
        }
        this.addChild(plane);

        updateWorld();

    }
}
