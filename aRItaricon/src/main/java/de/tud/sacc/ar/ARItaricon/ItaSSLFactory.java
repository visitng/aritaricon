package de.tud.sacc.ar.ARItaricon;


import java.io.InputStream;
import java.security.KeyStore;

import org.artoolkit.ar.samples.ARItaricon.R;

import android.content.Context;
import android.util.Log;

import javax.net.ssl.SSLContext;

import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLContexts;
import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.TrustSelfSignedStrategy;

/**
 * Class needed to sucessfully connect to The Itaricon Server, secured by HTTPS
 */
public class ItaSSLFactory {
	protected static final String TAG = "ItaSSLFactory";
    public static final SSLConnectionSocketFactory getSSLConnectionSocketFactory(final Context context) {
        SSLConnectionSocketFactory ret = null;

        try {


            // Trust own CA and all self-signed certs
            final KeyStore ks = KeyStore.getInstance("BKS");
            final InputStream inputStream = context.getResources().openRawResource(R.raw.itaricon);
            SSLContext sslcontext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                    .loadKeyMaterial(ks, context.getString(R.string.store_pass).toCharArray())
                    .useTLS()
                    .build();

            inputStream.close();

            ret = new SSLConnectionSocketFactory(
                    sslcontext,
                    new String[] { "TLSv1" },   // supported protocols
                    new String[] { "SRP-DSS-AES-256-CBC-SHA" },                       // supported cipher suites
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            //ret = new SSLConnectionSocketFactory(new SSLContext() ks);
            //ret.setHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }

        return ret;
    }

    public static SSLSocketFactory getSSLSocketFactory(final Context context) {
        SSLSocketFactory ret = null;
        
        try {
            final KeyStore ks = KeyStore.getInstance("BKS");
            final InputStream inputStream = context.getResources().openRawResource(R.raw.itakey);
            ks.load(inputStream, context.getString(R.string.store_pass).toCharArray());
            inputStream.close();
            ret = new SSLSocketFactory(ks);
            ret.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
 
        return ret;
    }

}
