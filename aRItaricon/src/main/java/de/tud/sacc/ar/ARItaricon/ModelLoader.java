package de.tud.sacc.ar.ARItaricon;

import android.content.res.AssetManager;
import android.util.Log;

import com.threed.jpct.Loader;
import com.threed.jpct.Object3D;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Singleton class that helps loading 3D Models from the asset directory
 * and stores instances of the models for later use
 */
public class ModelLoader {
    private static ModelLoader instance = null;

    private Map<String, Object3D[]> models = new HashMap<String, Object3D[]>();

    private ModelLoader() {
    }

    /**
     * loads and initializeds all models from a certain directory in
     * the asset folder
     *
     * @param mgr
     * @param path
     */
    public void initModelsFromAssets(AssetManager mgr, String path) {
        try {
            String list[] = mgr.list(path);
            if (list != null)
                for (int i=0; i<list.length; ++i) {
                        if(list[i].endsWith(".obj")) {
                            loadModel(mgr, path, list[i].substring(0, list[i].length()-4));
                        }
                    }
        } catch (IOException e) {
            Log.v("List error:", "can't list" + path);
        }
    }

    private void loadModel(AssetManager mgr, String path, String name) {
        try {
            Object3D[] object3D2 = Loader.loadOBJ(mgr.open(path + "/" + name + ".obj"), mgr.open(path + "/" + name + ".mtl"), 10);
            models.put(name, object3D2);
        }catch(final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * get a initialized 3D model
     *
     * @param name  name of the model (filename without extention)
     * @return      if the model exists, the model otherwise null
     */
    public Object3D[] getModel(String name) {
        if(!models.containsKey(name))
            return null;
        return  models.get(name);
    }

    public static ModelLoader getInstance() {
        if(instance ==  null)
            instance = new ModelLoader();
        return instance;
    }

}
