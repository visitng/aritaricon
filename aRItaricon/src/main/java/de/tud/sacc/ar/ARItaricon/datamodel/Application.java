package de.tud.sacc.ar.ARItaricon.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


/**
 * datamodel for an application in the Alfabet database
 */
public class Application {
	private final static String TAG = "models.Application";


	private String appID;
	private String appName;
	private Integer maxCrit = 0;
	private String name;
	private String firstname;
	private String email;
	private String phone;
	private String version;

	public Application(String _id, JSONObject obj) {
		this.appID = _id;
		updateFromJSON(obj);
	}
	/**
	 * Will set deviceName and stereotype of the application according to the
	 * JSON Structure passed to the function
	 * @param obj
	 */
	public void updateFromJSON(JSONObject obj) {
		
		this.appName = tryToApplyString("AppName", obj);
		this.name = tryToApplyString( "Name", obj);
		this.firstname = tryToApplyString("Firstname", obj);
		this.phone = tryToApplyString("Phone", obj);
		this.version = tryToApplyString( "Version", obj);
		this.email = tryToApplyString( "Email", obj);	
		this.maxCrit = tryToApplyInteger("MaxCrit", obj);
	}

	/**
	 * helper method to prevent an error if the JSON isn't
	 * in the intended format. Silently catches an error
	 * (but will log it)
	 *
	 * @param name
	 * @param obj
     * @return
     */

	//TODO: this should got into a static helper class
	private String tryToApplyString( String name, JSONObject obj) {
		String val = "";
		try {			
			val = obj.getString(name);
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing the Device JSON Object " + e.getMessage());
		}	
		return val;
	}

	//TODO: this should got into a static helper class
	private Integer tryToApplyInteger(String name, JSONObject obj) {
		Integer val = 0;
		try {
			val = obj.getInt(name);
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing the Device JSON Object " + e.getMessage());
		}
		return val;
	}
	
	public String getAppID() {
		return appID;
	}
	
	public String getAppName() {
		return appName;
	}
	
	public Integer getMaxCrit() {
		return maxCrit;
	}
	
	public String getName() {
		return name;
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPhone()  {
		return phone;
	}

}
