package de.tud.sacc.ar.ARItaricon;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.artoolkit.ar.samples.ARItaricon.ARSimpleApplication;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;
import de.tud.sacc.ar.ARItaricon.datamodel.Device;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import cz.msebera.android.httpclient.Header;

/**
 * Singleton class that handles the REST Connection to the Alfabet Database by using
 * an {@link AsyncHttpClient}
 */
public class AlfabetRestClient {
	private final String TAG = "AlfabetRestClient";
    protected AsyncHttpClient client;
	private Map<String, Device> devices = new HashMap<String, Device>();
    private static AlfabetRestClient instance = null;
	private List<DevicesUpdatedListener> listener = new ArrayList<DevicesUpdatedListener>();

	public Map<String, Device> getDevices() {
		return  devices;
	}

	public static AlfabetRestClient getInstance() {
		if(instance == null)
			instance = new AlfabetRestClient();
		return instance;
	}

	/**
	 * Initializes the  {@link client} by using a custom
	 * SSLSocketFactory
	 */
    private AlfabetRestClient() {
    	client = new AsyncHttpClient();//false, 80, 443);
    	final SSLSocketFactory sf = ItaSSLFactory.getSSLSocketFactory(ARSimpleApplication.getInstance().getApplicationContext());
    	if(sf != null) {
			client.setSSLSocketFactory(sf);
			Log.w(TAG, "Could not establish COnnection");
		} else
    		client = new AsyncHttpClient(false, 80, 443);

    }

    /**
     * Will request the REST-Service to get [host:port]/all, which should
     * respond the complete list of devices including applications. The devices
     * are then accessible by the {@link getDevices()} method
     * @return
     */
	public RequestHandle fetchDevices() {

        return client.get(getApiUrl("all"), new JsonHttpResponseHandler("UTF-8") {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONObject devicesObj = response.getJSONObject("Devices");
                    for(Iterator<String> devObjIter = devicesObj.keys(); devObjIter.hasNext();) {
                        String objId = devObjIter.next();
                        JSONObject deviceObj = devicesObj.getJSONObject(objId);
                        if(devices.containsKey(objId)) {
                            devices.get(objId).updateFromJSON(deviceObj);
                        } else {
                            devices.put(objId, new Device(objId, deviceObj));
                        }
                    }
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                }
                for(DevicesUpdatedListener l : listener) {
                    l.onUpdated(devices);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

        });

	}
	
	
    protected String getApiUrl(String path) {

        return ARItariconConfig.getInstance().getRestUrl() + path;

    }

	public void addListener(DevicesUpdatedListener l) {
		listener.add(l);
	}

	public interface DevicesUpdatedListener extends EventListener {
		void onUpdated(Map<String, Device> _devices);
	}
	

}
