#!/bin/bash

#BCPROV_JAR=/usr/share/java/bcprov.jar
BCPROV_JAR=./bcprov-jdk16-146.jar

if [ -z $1 -o -z $2 ];then
    echo "Usage:"
    echo "ssl-key-generation [HOST_NAME] [OUTPUT_FILE]"
    exit -1
fi

if [ ! -e ${BCPROV_JAR} ];then
    echo "no bcprov.jar has been found... please install libbcprov-java";
    exit -1
fi

HOST=$1
PORT=443
OUTPATH=$2

openssl s_client  -connect ${HOST}:${PORT} </dev/null 2>/dev/null|openssl x509 -outform PEM > /tmp/certfile.pem
keytool -importcert -v  -trustcacerts -file /tmp/certfile.pem -alias itaricon -keystore ${OUTPATH} -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath ${BCPROV_JAR} -storetype BKS
