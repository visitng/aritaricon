# ARItaricon - AR Application for Alfabet

## Setup

For this project to build you only need a recent gradle and
the android SDK

## Development

Recommended IDE is Android Studio >= 2.1
This will download all the necessary  Android components
like the Android SDK.
For C++/C development the native Android Developer Kit (NDK)
is needed . See [Android NDK](https://developer.android.com/ndk/index.html)
for further installation instructions.

Once the Android Studio has been launched, got to File -> Open and
navigate to the root of the project.

For the development and additional sample projects you shauld check out
the ARToolkit Library, either from [their server](http://artoolkit.org/download-artoolkit-sdk)
or [from Github](https://github.com/artoolkit/artoolkit5)
It is recommended to read through [their Android documentation](http://artoolkit.org/documentation/doku.php?id=4_Android:android_examples)

### Epson Moverio

There is a page at he ARToolkit site, which has some incomplete info
about the use of the toolkit with the Eposn Moverio-BT200. [here](http://artoolkit.org/documentation/doku.php?id=4_Android:android_bt-200)

The stereo-Branch of this repository, could be a starting point to bring
real 3D AR capabilities, but needs further investigation.

## Documentation

There is a gradle target called ''genJavadoc'', which builds the
documentation for this project.